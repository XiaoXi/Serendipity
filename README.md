<br />
<p align="center">
  <a href="https://serendipity.soraharu.com" target="_blank">
    <img src="images/logo.png" alt="Logo" width="156" height="156">
  </a>
  <h2 align="center" style="font-weight: 600">Serendipity</h2>

  <p align="center">
    高颜值的第三方网易云播放器
    <br />
    <a href="https://serendipity.soraharu.com" target="_blank"><strong>🌎 访问 DEMO</strong></a>&nbsp;&nbsp;|&nbsp;&nbsp;
    <a href="https://download.serendipity.soraharu.com/" target="_blank"><strong>📦️ 下载安装包</strong></a>
    <br />
    <br />
  </p>
</p>

## ✨ 特性

- ✅ 使用 Vue.js 全家桶开发
- 🔴 网易云账号登录（扫码 / 手机 / 邮箱登录）
- 📺 支持 MV 播放
- 📃 支持歌词显示
- 📻 支持私人 FM / 每日推荐歌曲
- 🚫🤝 无任何社交功能
- 🌎️ 海外用户可直接播放（需要登录网易云账号）
- 🔐 支持 [UnblockNeteaseMusic](https://github.com/UnblockNeteaseMusic/server#音源清单)，自动使用[各类音源](https://github.com/UnblockNeteaseMusic/server#音源清单)替换变灰歌曲链接 （网页版不支持）
  - 「各类音源」指默认启用的音源
  - YouTube 音源需自行安装 `yt-dlp`
- ✔️ 每日自动签到（手机端和电脑端同时签到）
- 🌚 Light / Dark Mode 自动切换
- 👆 支持 Touch Bar
- 🖥️ 支持 PWA，可在 Chrome/Edge 里点击地址栏右边的 ➕ 安装到电脑
- 🟥 支持 Last.fm Scrobble
- ☁️ 支持音乐云盘
- ⌨️ 自定义快捷键和全局快捷键
- 🎧 支持 Mpris
- 🛠 更多特性开发中

## 📦️ 安装

Electron 版本，支持 macOS、Windows、Linux。

访问 [Serendipity 官网](https://download.serendipity.soraharu.com/) 或本项目的 [Release](https://gitlab.soraharu.com/XiaoXi/Serendipity/-/releases) 页面下载安装包。

## ⚙️ 部署至 Vercel

除了下载安装包使用，你还可以将本项目部署到 Vercel 或你的服务器上。下面是部署到 Vercel 的方法：

1. 部署网易云 API，详情参见 [Binaryify/NeteaseCloudMusicApi](https://neteasecloudmusicapi.vercel.app/#/?id=%e5%ae%89%e8%a3%85)，你也可以将 API 部署到 Vercel

2. 点击本仓库右上角的 Fork，复制本仓库到你的 GitHub 账号。

3. 点击仓库的 `Add File`，选择 `Create new file`，输入 `vercel.json`，将下面的内容复制粘贴到文件中，并将 `https://your-netease-api.example.com` 替换为你刚刚部署的网易云 API 地址：

```json
{
  "rewrites": [
    {
      "source": "/api/:match*",
      "destination": "https://your-netease-api.example.com/:match*"
    }
  ]
}
```

4. 打开 [Vercel.com](https://vercel.com/)，使用 GitHub 或 GitLab 账号登录

5. 点击 `Import Git Repository` 并选择你刚刚复制的仓库并点击 `Import`

6. 点击 PERSONAL ACCOUNT 旁边的 Select

7. 点击 `Environment Variables`，填写 Name 为 `VUE_APP_NETEASE_API_URL`，Value 为 `/api`，点击 Add

8. 最后点击底部的 `Deploy` 就可以将本项目部署到 Vercel

## ⚙️ 部署到自己的服务器

除了部署到 Vercel，你还可以部署到自己的服务器上：

1. 部署网易云 API，详情参见 [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi)
2. 克隆本仓库

```sh
git clone --recursive https://gitlab.soraharu.com/XiaoXi/Serendipity.git
```

3. 安装依赖

```sh
yarn install
```

4. （可选）使用 Nginx 反向代理 API，将 API 路径映射为 `/api`，如果 API 和网页不在同一个域名下的话可能会存在跨域的问题

5. 复制 `/.env.example` 文件为 `/.env`，修改里面 `VUE_APP_NETEASE_API_URL` 的值为网易云 API 地址。本地开发的话可以填写 API 地址为 `http://localhost:3000`，Serendipity 地址为 `http://localhost:8080`。如果你使用了反向代理 API，可以填写 API 地址为 `/api`

```
VUE_APP_NETEASE_API_URL=http://localhost:3000
```

6. 编译打包

```sh
yarn run build
```

7. 将 `/dist` 目录下的文件上传到你的 Web 服务器

## ⚙️ Docker 部署

1. 构建 Docker Image

```sh
docker build -t serendipity .
```

2. 启动 Docker Container

```sh
docker run -d --name Serendipity -p 80:80 serendipity
```

3. Docker Compose 启动

```sh
docker-compose up -d
```

Serendipity 地址为 `http://localhost`

## ⚙️ 部署至 Replit

1. 新建 Repl，选择 Bash 模板

2. 在 Replit shell 中运行以下命令

```sh
bash <(curl -s -L https://gitlab.soraharu.com/XiaoXi/Serendipity/-/blob/master/install-replit.sh)
```

3. 首次运行成功后，只需点击绿色按钮 `Run` 即可再次运行

4. 由于 Replit 个人版限制内存为 1G（教育版为 3G），构建过程中可能会失败，请再次运行上述命令或运行以下命令：

```sh
cd /home/runner/${REPL_SLUG}/music && yarn installl && yarn run build
```

## 👷‍♂️ 打包客户端

如果在 Release 页面没有找到适合你的设备的安装包的话，你可以根据下面的步骤来打包自己的客户端。

1. 打包 Electron 需要用到 Node.js 和 Yarn。可前往 [Node.js 官网](https://nodejs.org/zh-cn/) 下载安装包。安装 Node.js 后可在终端里执行 `npm install -g yarn` 来安装 Yarn

2. 使用 `git clone --recursive https://gitlab.soraharu.com/XiaoXi/Serendipity.git` 克隆本仓库到本地

3. 使用 `yarn install` 安装项目依赖

4. 复制 `/.env.example` 文件为 `/.env`

5. 选择下列表格的命令来打包适合你的安装包，打包出来的文件在 `/dist_electron` 目录下。了解更多信息可访问 [electron-builder 文档](https://www.electron.build/cli)

| Command                                    | Lore                      |
| ------------------------------------------ | ------------------------- |
| `yarn electron:build --windows nsis:ia32`  | Windows 32bit             |
| `yarn electron:build --windows nsis:arm64` | Windows ARM               |
| `yarn electron:build --linux deb:armv7l`   | Debian armv7l (raspberry) |
| `yarn electron:build --macos dir:arm64`    | macOS ARM                 |

## 💻 配置开发环境

本项目由 [NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi) 提供 API

运行本项目

```shell
# 安装依赖
yarn install

# 创建本地环境变量
cp .env.example .env

# 运行（网页端）
yarn serve

# 运行（electron）
yarn electron:serve
```

本地运行 NeteaseCloudMusicApi，或者将 API 部署至 Vercel

```shell
# 运行 API （默认 3000 端口）
yarn netease_api:run
```

## 📜 开源许可

本项目仅供个人学习研究使用，禁止用于商业及非法用途。

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。

## 💕 灵感来源

API 源代码来自 [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi)

- [Apple Music](https://music.apple.com)
- [YouTube Music](https://music.youtube.com)
- [Spotify](https://www.spotify.com)
- [网易云音乐](https://music.163.com)
